# thanks to dima for walking me through this!
#
# needs: apt install emacs texlive-latex-extra librsvg2-bin graphviz

export FORCE_SOURCE_DATE = 1
export SOURCE_DATE_EPOCH := $(shell date --utc --date '2020-08-23 22:30 UTC' +%s)

all: $(patsubst %.org,%.pdf,$(wildcard *.org))

%.pdf: %.org squigglyrainbowlines.png
	emacs -Q --batch --eval '(progn (random "0") (find-file "$<") (org-beamer-export-to-pdf))'

squigglyrainbowlines.png:
	rsvg-convert --output=squigglyrainbowlines.png images/squigglyrainbowlines.svg

clean:
	rm -f *.pdf *.tex *.png

.PHONY:clean
